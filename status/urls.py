from django.urls import path

from . import views
from api.views import TimeYiew

urlpatterns = [
    path('', views.FormDisplay.as_view(), name='display'),
    path('time/', TimeYiew.as_view()),
]
