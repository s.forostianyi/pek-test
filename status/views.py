import json

from datetime import datetime
from pytz import timezone

from django.shortcuts import render
from django.views.generic import View



class FormDisplay(View):
    def get(self, request):

        europe = timezone('Europe/Amsterdam')
        eu_time = datetime.now(europe)
        now = eu_time.strftime('%H:%M:%S')

        with open('data.json') as f:
            data = json.load(f)

        return render(request, 'status/status.html',
                      {"speed": data['current_speed'], "types": data['types'], 'date': now})


