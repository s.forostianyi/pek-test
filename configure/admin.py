from django.contrib import admin

from .models import Type, ConfigurationField, Speed

class ConfigurationAdmin(admin.ModelAdmin):
    readonly_fields = ('name', 'choosed_speed', 'types', 'deleted')

admin.site.register(Type)
admin.site.register(ConfigurationField, ConfigurationAdmin)
admin.site.register(Speed)
