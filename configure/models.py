from django.db import models
from django.contrib.auth.models import User

class Type(models.Model):

    name = models.CharField("Name", max_length=150)
    created = models.DateTimeField('Date created', auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(auto_now=False, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = """type"""
        verbose_name_plural = """types"""

class Speed(models.Model):

    speed = models.PositiveSmallIntegerField("Speed")

    created = models.DateTimeField('Date created', auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(auto_now=False, blank=True, null=True)

    def __str__(self):
        return str(self.speed) + ' km/h'

    class Meta:
        verbose_name = """speed"""
        verbose_name_plural = """speed"""

class ConfigurationField(models.Model):
    name = models.CharField("Name", max_length=150)
    choosed_speed = models.ForeignKey(Speed, on_delete=models.CASCADE, blank=False, null=True)
    types = models.ManyToManyField(Type, blank=False)

    created = models.DateTimeField('Date created', auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(auto_now=False, blank=True, null=True)


    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = """configuration"""
        verbose_name_plural = """configurations"""
