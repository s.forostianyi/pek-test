from django.urls import path

from . import views

urlpatterns = [
    path('', views.FormConfiguration.as_view(), name='configure'),
]
