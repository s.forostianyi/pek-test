from django import forms

class ConfigurationForm(forms.Form):
    speed = forms.CharField(max_length=10)
    types = forms.CharField(max_length=10)
