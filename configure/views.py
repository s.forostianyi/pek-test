from django.shortcuts import render
from django.views.generic import View
from .models import Speed, Type, ConfigurationField
from .forms import ConfigurationForm

import json
from datetime import datetime
from pytz import timezone


class FormConfiguration(View):
    def get(self, request):
        speeds = Speed.objects.all()
        types = Type.objects.all()
        return render(request, 'configure/configure.html', {'speeds': speeds, 'types': types})

    def post(self, request):
        configuration = ConfigurationForm(request.POST)
        if configuration.is_valid():
            data_speed = request.POST['speed']
            data_types = request.POST['types']

            array_types = []
            array_id_types = []
            for data_type in data_types:
                if data_type != ',':
                    array_id_types.append(data_type)
                    types = Type.objects.filter(id=data_type).values('name')
                    for type in types:
                        array_types.append(type)
            query = Speed.objects
            speed = query.get(id=data_speed).speed
            choosed_speed = query.get(speed=speed)

            europe = timezone('Europe/Amsterdam')
            eu_time = datetime.now(europe)
            now = eu_time.strftime('%H:%M:%S %d-%m-%Y')
            name = 'Configuration ' + now

            ConfigurationField.objects.create(name=name, choosed_speed=choosed_speed)

            query = ConfigurationField.objects.get(name=name).types
            long = len(array_id_types)
            i = 0
            while i < long:
                query.add(int(array_id_types[i]))
                i += 1

            to_json = {'date': now, 'types': array_types, 'current_speed': speed}

            with open('data.json', 'w') as f:
                f.write(json.dumps(to_json))

            return render(request, 'choose/index.html')
        else:
            return render(request, 'choose/index.html')
