from django.urls import path

from . import views

urlpatterns = [
    path('', views.FormChoose.as_view(), name='index'),
]
