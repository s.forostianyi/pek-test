from django.http import HttpResponse
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import render

from datetime import datetime
from pytz import timezone

from .serializers import UserSerializer, LoginSerializer
from rest_framework.permissions import AllowAny
from rest_framework import status


class UserCreate(CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerializer


class LoginUser(APIView):
    """
    Logs in an existing user.
    """
    permission_classes = [AllowAny]
    serializer_class = LoginSerializer

    def post(self, request):
        """
        Checks is user exists.
        Email and password are required.
        Returns a JSON web token.
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class TimeYiew(APIView):
    def get(self, request):
        europe = timezone('Europe/Amsterdam')
        eu_time = datetime.now(europe)
        now = eu_time.strftime('%H:%M:%S')
        return HttpResponse(now)
