function refresh() {
    var settings = {
        "url": "http://10.6.0.50/api/time/",
        "method": "GET",
        "timeout": 0,
        "headers": {
            "Authorization": "Token 552040483c9328d75d26f54cb9c09ad505593aa3"
        },
    };
    $.ajax(settings).done(function (response) {
        $('.refresh').html(response);
    });
}

$(function () {
    setInterval(refresh, 5000);
});