$(function () {
    $(document).ready(function () {
        var $eventMultySelect = $('.js-multyselect-event');
        $eventMultySelect.on('select2:select select2:unselect', function () {
            var types = $(this).val()
            $('.type_array').val(types);
        });
        $eventMultySelect.select2({
            placeholder: "Choose types",
            maximumSelectionLength: 3,
            language: "en"
        });

        var $eventSingleSelect = $('.js-singlselect-event');
        $eventSingleSelect.select2().val('').trigger("select2:select")
        $eventSingleSelect.on('select2:select select2:unselect', function () {
            var speed = $(this).val()
            $('.speed_array').val(speed);
        });
        $eventSingleSelect.select2({
            placeholder: "Choose speed",
            language: "en"
        });
    });
});
