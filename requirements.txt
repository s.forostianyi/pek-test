Django==3.2.6
psycopg2-binary==2.9.1
djangorestframework
django-rest-auth==0.9.5
django-filter
django-cors-headers
django-bootstrap-v5
